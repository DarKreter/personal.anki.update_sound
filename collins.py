import requests, os
from bs4 import BeautifulSoup

from secret import COLLINS_API_KEY


class CollinsAPI:
    def __init__(self, word):
        if COLLINS_API_KEY == "" or self._check_limit() == 0:
            self.word = self.html = self.pronunciation = self.audio = self.meanings = None
        else:
            self.word = word
            self.html = self._get_html()
            self.pronunciation = self._get_pronunciation()
            self.audio = self._get_audio()
            self.meanings = self._get_meanings()

    def _check_limit(self):
        filename = "./collins_limit.txt"
        if os.path.exists(filename):
            with open(filename, "r+") as f:
                number = int(f.read())
                if number != 0:
                    number -= 1
                    f.seek(0)
                    f.write(str(number))
                    f.close()
                    return number + 1
                else:
                    print("Collins API monthly request reached! ")
                    return 0
        else:
            print("No limitation file for collins, allowing request!")
            return 1

    def _get_html(self):
        # print("Making Collins request!")
        url = "https://api.collinsdictionary.com/api/v1/dictionaries/english/search/first/"
        headers = {
            "Accept": "application/json",
            "accessKey": COLLINS_API_KEY,
        }
        params = {"q": f"{self.word}", "format": "json"}
        response = requests.get(url, headers=headers, params=params)
        # print(response.json())
        if response.ok:
            return response.json()["entryContent"]
        else:
            return None

    def _get_pronunciation(self):
        if self.html:
            soup = BeautifulSoup(self.html, "html.parser")
            tag = soup.find("span", class_="pron")
            if tag:
                return tag.text.split("Your browser", 1)[0]
        return None

    def _get_audio(self):
        if self.html:
            soup = BeautifulSoup(self.html, "html.parser")
            audio_tag = soup.find("audio")
            if audio_tag:
                return audio_tag.find("source")["src"]
        return None

    def _download_audio(self, file_name):
        if self.audio:
            audio_url = self.audio
            response = requests.get(audio_url)
            with open(f"{file_name}", "wb") as f:
                f.write(response.content)

    def _get_meanings(self):
        meanings = []
        if self.html:
            soup = BeautifulSoup(self.html, "html.parser")
            for def_div in soup.find_all("span", class_="def"):
                meaning = def_div.text
                meanings.append(meaning)
        return meanings[:2]


# word = CollinsAPI("swole")
# print(word.pronunciation)
# print(word.audio)
# print(word.meanings)
# word.download_audio("./config/omnivorous.mp3")
