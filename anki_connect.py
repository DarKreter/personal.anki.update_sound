import json
import urllib.request
from time import sleep
from secret import SOUND_FIELD, PATH

# Based on anki_connect Addon API
def request(action, **params):
    return {"action": action, "params": params, "version": 6}


def invoke(action, **params):
    requestJson = json.dumps(request(action, **params)).encode("utf-8")
    # print(requestJson)
    response = json.load(
        urllib.request.urlopen(urllib.request.Request("http://localhost:8765", requestJson))
    )
    # print(response)
    if len(response) != 2:
        raise Exception("response has an unexpected number of fields")
    if "error" not in response:
        raise Exception("response is missing required error field")
    if "result" not in response:
        raise Exception("response is missing required result field")
    if response["error"] is not None:
        raise Exception(response["error"])
    return response["result"]


def update_sound(note_id, sound_file_name):
    _note = {
        "id": note_id,
        "fields": {SOUND_FIELD: ""},
        "audio": [
            {
                "filename": "sound.mp3",
                "path": f"{PATH}{sound_file_name}",
                "skipHash": "123",
                "fields": [SOUND_FIELD],
            }
        ],
    }

    result = invoke("updateNoteFields", note=_note)
    return result


def get_notes(deck):
    return invoke("findNotes", query="deck:{}".format(deck))


def get_note_info(note_id):
    return invoke("notesInfo", notes=[note_id])


def sync():
    invoke("sync")
    sleep(1)
