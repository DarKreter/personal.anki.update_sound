from time import sleep
import os

from anki_connect import update_sound, sync, get_notes, get_note_info
from secret import DECK, PHRASE_FIELD, TYPE
from TTS import text_to_speech
from collins import CollinsAPI

print("Syncing!")
sync()
try:
    print(f"Starting updating sound for deck: {DECK}")
    for note_id in get_notes(DECK):
        word = get_note_info(note_id)[0]["fields"][PHRASE_FIELD]["value"]
        print("{:<30}".format(word), end="")

        file_name = "word.mp3"
        _type = TYPE
        if _type == "COLLINS":
            obj = CollinsAPI(word)
            if obj._get_audio() == None:
                _type = "AWS"
            else:
                obj._download_audio(file_name)
        if _type == "AWS":
            text_to_speech(word, file_name)
            
        # sleep(1)
        response = update_sound(note_id, file_name)
        # if file_name != "":
        #     os.remove(file_name)
        print(f"DONE! ({_type})")

    print("Success!")
except Exception as e:
    print("Error!")
    print(e)

sync()
input("Press any key to continue...")
