import boto3
from secret import AWS_KEY_ID, AWS_SECRET_ACCESS_KEY


def text_to_speech(word, file_name):
    if AWS_KEY_ID == "":
        return -1
    client = boto3.client(
        "polly",
        aws_access_key_id=AWS_KEY_ID,
        aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
        region_name="us-east-1",
    )

    output = client.synthesize_speech(Text=word, OutputFormat="mp3", VoiceId="Salli")

    # Save the audio stream to an MP3 file
    with open(file_name, "wb") as f:
        f.write(output["AudioStream"].read())

    return 0
